import { skipWaiting, clientsClaim } from 'workbox-core'
import { Prefetcher, prefetch } from '@layer0/prefetch/sw'
import DeepFetchPlugin, { DeepFetchCallbackParam } from '@layer0/prefetch/sw/DeepFetchPlugin'
import cacheHost from './cacheHost'

skipWaiting()
clientsClaim()

new Prefetcher({
  cacheHost,
  plugins: [
    new DeepFetchPlugin([
      {
        selector: '.product-gallery__img img',
        maxMatches: 1,
        attribute: 'src',
        as: 'image',
      },
      {
        selector: '.product-card__image > .picture__img',
        maxMatches: 4,
        attribute: 'src',
        as: 'image',
        callback: deepFetchResponsiveImages,
      },
    ]),
  ],
})
  .route()
  .cache(/^https:\/\/cdn\.shopify\.com\/.*/)

function deepFetchResponsiveImages({ $el, el, $ }: DeepFetchCallbackParam) {
  const $source = $el.prev()
  const srcset = $source.attr('data-srcset')

  if (srcset) {
    for (let src of srcset.slice(-2)) {
      const url = src.trim().split(' ')[0]
      prefetch(url, 'image')
    }
  }
}
