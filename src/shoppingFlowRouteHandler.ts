import { CACHE_PAGES } from './cache'
import { RouteHandler } from '@layer0/core/router/Router'

const handler: RouteHandler = async ({
  cache,
  removeUpstreamResponseHeader,
  updateResponseHeader,
  proxy,
}) => {
  removeUpstreamResponseHeader('set-cookie')
  cache(CACHE_PAGES)
  proxy('origin')
  updateResponseHeader('location', /https:\/\/base-dev.myshopify.com\//gi, '/') // convert absolute redirects to origin to relative so that the user isn't transferred to the origin.
}

export default handler
