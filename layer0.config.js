// This file was automatically added by layer0 deploy.
// You should commit this file to source control.
module.exports = {
  routes: './src/routes.ts',
  backends: {
    origin: {
      domainOrIp: 'base-dev.myshopify.com',
      hostHeader: 'base-dev.myshopify.com',
    },
  },
}
